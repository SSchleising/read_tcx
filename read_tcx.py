import xml.etree.ElementTree as et
from datetime import datetime

import pandas as pd
import numpy as np

from bokeh.plotting import figure, output_file, save, show
from bokeh.layouts import column
from bokeh.models import ColumnDataSource, HoverTool, GeoJSONDataSource
from bokeh.tile_providers import Vendors, get_provider

tools = 'pan, wheel_zoom, reset'

# Deal with the godawful namespace
namespace  = '{http://www.garmin.com/xmlschemas/TrainingCenterDatabase/v2}'

Activities = namespace + 'Activities'
Activity   = namespace + 'Activity'
Lap        = namespace + 'Lap'
Track      = namespace + 'Track'
Trackpoint = namespace + 'Trackpoint'
Time       = namespace + 'Time'
Position   = namespace + 'Position'
Latitude   = namespace + 'Position/' + namespace + 'LatitudeDegrees'
Longitude  = namespace + 'Position/' + namespace + 'LongitudeDegrees'
Altitude   = namespace + 'AltitudeMeters'
Distance   = namespace + 'DistanceMeters'

filename = 'data/exercise_tcx_file.tcx'

# Function to convert Lat, Long to Web Mercator
def geographic_to_web_mercator(lat, long):
    num = long * 0.017453292519943295
    x = 6378137.0 * num
    a = lat * 0.017453292519943295
    x_mercator = x
    y_mercator = 3189068.5 * np.log((1.0 + np.sin(a)) / (1.0 - np.sin(a)))
    return x_mercator, y_mercator

def ParseTcxFile():
    # Create a dataframe for the date
    df = pd.DataFrame()

    # Lists to hold data as we iterate through
    times     = list()
    lats      = list()
    longs     = list()
    alts      = list()
    distances = list()

    # I have nothing good to say about any of this
    xtree = et.parse(filename)
    xroot = xtree.getroot()

    # Loop through the various horrifically nested elements of the xml file
    for activities in xroot.findall(Activities):
        for activity in activities.findall(Activity):
            for lap in activity.findall(Lap):
                for track in lap.findall(Track):
                    for trackpoint in track.findall(Trackpoint):
                        # Get the data from the trackpoints
                        times.append(datetime.fromisoformat(trackpoint.find(Time).text))
                        lats.append(float(trackpoint.find(Latitude).text))
                        longs.append(float(trackpoint.find(Longitude).text))
                        alts.append(float(trackpoint.find(Altitude).text))
                        distances.append(float(trackpoint.find(Distance).text))

    # Add the data to the dataframe
    df['time']      = times
    df['latitude']  = lats
    df['longitude'] = longs
    df['altitude']  = alts
    df['distance']  = distances

    # Get names of indexes for which column distance is 0
    indexNames = df[df['distance'] == 0].index

    # Delete these row indexes from dataFrame
    df.drop(indexNames , inplace=True)

    # Create a rolling average for altitude to smooth out any blips
    rolling_window = df.rolling(50)
    df['average_altitude'] = rolling_window.mean()['altitude']

    # Convert the lat/longs to Webmercator x/y
    df['WM_x'], df['WM_y'] = geographic_to_web_mercator(df['latitude'], df['longitude'])

    return df

def PlotTrack(df):
    # Get the tile provider
    tile_provider = get_provider(Vendors.WIKIMEDIA)

    # Set the padding around the logged data
    x_min = df['WM_x'].min() - 1000
    x_max = df['WM_x'].max() + 1000
    y_min = df['WM_y'].min() - 1000
    y_max = df['WM_y'].max() + 1000

    # Create the plot
    plot = figure(x_range=(x_min, x_max), 
                  y_range=(y_min, y_max),
                  x_axis_type="mercator", 
                  y_axis_type="mercator",
                  title='Walk', 
                  tools = tools,
                  active_drag   = 'pan',
                  active_scroll = 'wheel_zoom')

    # Add the tile streamer to the plot
    plot.add_tile(tile_provider)

    # Set the sizing mode
    plot.sizing_mode = 'stretch_both'

    # Setup the tooltips
    tooltips = [('Latitude', '@latitude'),
                ('Longitude', '@longitude'),
                ('Time', '@time{%c}'),
                ('Distance', '@distance'),
                ('Altitude', '@altitude')]

    formatter = { '@time' : 'datetime' }

    # Create a hover tool
    hover_tool = HoverTool()

    # Set the tooltips
    hover_tool.tooltips = tooltips
    hover_tool.formatters = formatter
    
    # Add the tooltip
    plot.add_tools(hover_tool)

    # Create a column data source from the dataframe
    cds = ColumnDataSource.from_df(df)

    # Add the data to the circle plot
    plot.line(x='WM_x', 
              y='WM_y',
              source=cds)

    return plot

def PlotAltitude(df):
    # Create the plot
    plot = figure(title='Altitude', 
                  tools = tools,
                  height = 250,
                  active_drag   = 'pan',
                  active_scroll = 'wheel_zoom')

    # Set the sizing mode
    plot.sizing_mode = 'stretch_width'

    # Setup the tooltips
    tooltips = [('Latitude', '@latitude'),
                ('Longitude', '@longitude'),
                ('Time', '@time{%c}'),
                ('Distance', '@distance'),
                ('Altitude', '@average_altitude')]

    formatter = { '@time' : 'datetime' }

    # Create a hover tool
    hover_tool = HoverTool()

    # Set the tooltips
    hover_tool.tooltips = tooltips
    hover_tool.formatters = formatter
    
    # Add the tooltip
    plot.add_tools(hover_tool)

    # Create a column data source from the dataframe
    cds = ColumnDataSource.from_df(df)

    # Add the data to the circle plot
    plot.line(x='distance', 
              y='average_altitude',
              source=cds)

    return plot

def main():
    # Set the output filename and title of the plot
    output_file('index.html', 'Walk')

    # Read the tcx file into a dataframe
    df = ParseTcxFile()

    # Create the plot
    track_plot = PlotTrack(df)

    # Create the altitude plot
    alt_plot = PlotAltitude(df)

    # Create a column to hold the plots
    Column = column(track_plot, alt_plot)

    # Set the columns resize mode
    Column.sizing_mode = 'stretch_both'

    # Show the plot
    show(Column)

# Call main()
main()